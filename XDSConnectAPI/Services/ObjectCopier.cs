﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace XDSConnectAPI.Services
{
    public class ObjectCopier
    {

        public static object Copy(object fromObj, object toObj, object fromobjValue, object toobjValue)
        {
            Type fromObjectType = fromObj.GetType();
            Type toObjectType = null;

            System.Reflection.PropertyInfo propertyInfo = (System.Reflection.PropertyInfo)toobjValue;

            //if (toObj == null)
            //{
            //    Activator.CreateInstance(toobjValue.GetType());
            //}
            if (toObj == null)
            {
                //toObj = propertyInfo.PropertyType.Assembly.CreateInstance(propertyInfo.Name);
                if (fromObjectType.IsArray)
                {


                    System.Collections.IEnumerable enumerable = fromObj as System.Collections.IEnumerable;
                    if (enumerable != null)
                    {
                        int count = 0;
                        foreach (object element in enumerable)
                        {
                            count++;
                        }

                        toObj = Activator.CreateInstance(propertyInfo.PropertyType, count);
                        toObjectType = toObj.GetType();
                        object[] x = (object[])toObj;
                        int i = 0;

                        foreach (object element in enumerable)
                        {
                            if (element != null)
                            {
                                Type oChildType = toObjectType.Assembly.GetType(toObjectType.FullName.Replace("[]", string.Empty));

                            x[i] = Activator.CreateInstance(oChildType, System.Reflection.BindingFlags.CreateInstance, null, null, null);

                           
                                foreach (System.Reflection.PropertyInfo fromProperty in
               element.GetType().GetProperties())
                                {

                                    if (fromProperty.CanRead)
                                    {
                                        string propertyName = fromProperty.Name;
                                        Type propertyType = fromProperty.PropertyType;

                                        System.Reflection.PropertyInfo toProperty =
                                            x[i].GetType().GetProperty(propertyName);

                                        if (toProperty != null)
                                        {
                                            Type toPropertyType = toProperty.PropertyType;

                                            if (toProperty != null && toProperty.CanWrite)
                                            {
                                                object fromValue = fromProperty.GetValue(element, null);

                                                if (fromValue != null)
                                                {
                                                    if (propertyType == toPropertyType)
                                                    {
                                                        toProperty.SetValue(x[i], fromValue, null);
                                                    }

                                                    else if (toProperty.PropertyType.IsEnum)
                                                    {
                                                        //if (toPropertyType.ToString().ToLower() == "enumresponsestatus")
                                                        //{
                                                        //    if(fromValue.ToString().ToLower() == "success")
                                                        //    {
                                                        toProperty.SetValue(x[i], fromValue.ToString().ToUpper(), null);
                                                        //    }
                                                        //}

                                                    }
                                                    else if (toProperty.PropertyType.Name.ToLower() == "datetime")
                                                    {
                                                        DateTime dt = new DateTime();
                                                        if (DateTime.TryParse(fromValue.ToString(), out dt))
                                                        {
                                                            toProperty.SetValue(x[i], dt, null);
                                                        }
                                                    }
                                                    else if (toProperty.PropertyType.Name.ToLower().Contains("nullable") && toProperty.PropertyType.FullName.ToLower().Contains("datetime"))
                                                    {
                                                        DateTime? dtV = null;

                                                        try
                                                        {
                                                            dtV = Convert.ToDateTime(fromValue.ToString());
                                                            toProperty.SetValue(x[i], dtV, null);
                                                        }
                                                        catch { }
                                                    }
                                                    else if (toProperty.PropertyType.Name.ToLower() == "decimal")
                                                    {
                                                        Decimal dc = 0;
                                                        if (Decimal.TryParse(fromValue.ToString(), out dc))
                                                        {
                                                            toProperty.SetValue(x[i], dc, null);
                                                        }
                                                    }
                                                    else if (toProperty.PropertyType.Name.ToLower().Contains("int"))
                                                    {
                                                        int it = 0;
                                                        if (int.TryParse(fromValue.ToString(), out it))
                                                        {
                                                            toProperty.SetValue(x[i], it, null);
                                                        }
                                                    }
                                                    else if (toProperty.PropertyType.Name.ToLower() == "boolean")
                                                    {
                                                        bool dData = false;
                                                        if (bool.TryParse(fromValue.ToString(), out dData))
                                                        {
                                                            toProperty.SetValue(x[i], dData, null);
                                                        }
                                                    }
                                                    else if (toProperty.DeclaringType.BaseType.Name.ToLower() == "object")
                                                    {
                                                        bool proceed = true;

                                                        //if (fromProperty.PropertyType.IsArray)
                                                        //{
                                                        //    System.Collections.IEnumerable childarray = fromProperty as System.Collections.IEnumerable;

                                                        //    if(childarray != null)
                                                        //    {
                                                        //        proceed = true;
                                                        //    }
                                                        //}

                                                        if (fromProperty.GetValue(element, null) != null)
                                                        {
                                                            proceed = true;

                                                        }
                                                        else
                                                        {
                                                            proceed = false;
                                                        }
                                                        if (proceed)
                                                        {
                                                            toProperty.SetValue(x[i], Copy(fromProperty.GetValue(element, null), toProperty.GetValue(x[i], null), fromProperty, toProperty), null);
                                                        }
                                                    }

                                                }
                                            }
                                        }
                                    }

                                }
                            }

                            i++;
                        }


                    }


                    return toObj;
                }
                else
                {
                    toObj = Activator.CreateInstance(propertyInfo.PropertyType, System.Reflection.BindingFlags.CreateInstance, null, null, null);


                }
            }

            toObjectType = toObj.GetType();
            foreach (System.Reflection.PropertyInfo fromProperty in
                fromObjectType.GetProperties())
            {
                if (fromProperty.CanRead)
                {
                    string propertyName = fromProperty.Name;
                    Type propertyType = fromProperty.PropertyType;

                    System.Reflection.PropertyInfo toProperty =
                        toObjectType.GetProperty(propertyName);
                    if (toProperty != null)
                    {
                        Type toPropertyType = toProperty.PropertyType;

                        if (toProperty != null && toProperty.CanWrite)
                        {
                            object fromValue = fromProperty.GetValue(fromObj, null);

                            if (fromValue != null)
                            {
                                if (propertyType == toPropertyType)
                                {
                                    toProperty.SetValue(toObj, fromValue, null);
                                }
                                else if (toProperty.PropertyType.IsEnum)
                                {
                                    //if (toPropertyType.ToString().ToLower() == "enumresponsestatus")
                                    //{
                                    //    if(fromValue.ToString().ToLower() == "success")
                                    //    {
                                    toProperty.SetValue(toObj, Enum.Parse(toProperty.PropertyType, fromValue.ToString().ToUpper(), true), null);
                                    //    }
                                    //}

                                }
                                else if (toProperty.PropertyType.BaseType.Name.ToLower() == "datetime")
                                {
                                    DateTime dt = new DateTime();
                                    if (DateTime.TryParse(fromValue.ToString(), out dt))
                                    {
                                        toProperty.SetValue(toObj, dt, null);
                                    }
                                }
                                else if (toProperty.PropertyType.Name.ToLower().Contains("nullable") && toProperty.PropertyType.FullName.ToLower().Contains("datetime"))
                                {
                                    DateTime? dtV = null;

                                    try
                                    {
                                        dtV = Convert.ToDateTime(fromValue.ToString());
                                        toProperty.SetValue(toObj, dtV, null);
                                    }
                                    catch { }
                                }
                                else if (toProperty.PropertyType.Name.ToLower() == "decimal")
                                {
                                    decimal dc = 0;
                                    if (decimal.TryParse(fromValue.ToString(), out dc))
                                    {
                                        toProperty.SetValue(toObj, dc, null);
                                    }
                                }
                                else if (toProperty.PropertyType.Name.ToLower().Contains("int"))
                                {
                                    int it = 0;
                                    if (int.TryParse(fromValue.ToString(), out it))
                                    {
                                        toProperty.SetValue(toObj, it, null);
                                    }
                                }
                                else if (toProperty.PropertyType.Name.ToLower() == "boolean")
                                {
                                    bool dData = false;
                                    if (bool.TryParse(fromValue.ToString(), out dData))
                                    {
                                        toProperty.SetValue(toObj, dData, null);
                                    }
                                }
                                else if (toProperty.DeclaringType.BaseType.Name.ToLower() == "object")
                                {
                                    bool proceed = true;

                                    //if (fromProperty.PropertyType.IsArray)
                                    //{
                                    //    System.Collections.IEnumerable childarray = fromProperty as System.Collections.IEnumerable;

                                    //    if (childarray != null)
                                    //    {
                                    //        proceed = true;
                                    //    }
                                    //}

                                    if (fromProperty.GetValue(fromObj, null) != null)
                                    {
                                        proceed = true;

                                    }
                                    else
                                    {
                                        proceed = false;
                                    }
                                    if (proceed)
                                    {
                                        toProperty.SetValue(toObj, Copy(fromProperty.GetValue(fromObj, null), toProperty.GetValue(toObj, null), fromProperty, toProperty), null);
                                    }
                                }
                            }
                        }
                    }
                }


            }


            return toObj;
        }
    }


}