﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using XDSConnectAPI.Models;
namespace XDSConnectAPI.Services
{
    public class LoginService
    {
        public ConnectTicket LoginVerification(string Username, string Password)
        {
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
            XDSConnect.XDSConnectWS xdsConnect = new XDSConnect.XDSConnectWS();
            string Result = xdsConnect.Login(Username, Password);

            ConnectTicket connectTicket = new ConnectTicket();

            if (Result == "UserNotFound" || Result == "NotAuthenticated")
            {
                connectTicket.Error = Result;
            }
            else
            {
                connectTicket.Value = Result;
            }

            return connectTicket;
        }
    }
}