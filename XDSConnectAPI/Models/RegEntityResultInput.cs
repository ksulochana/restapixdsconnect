﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace XDSConnectAPI.Models
{
    public class RegEntityResultInput
    {

        public int ProductCode { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
        public string BureauNumber { get; set; }
        public string EnquiryReason { get; set; }
        public string EnquiryAmount { get; set; }
        public string Terms { get; set; }
        public string UserOnTicket { get; set; }
        public string ContactForename { get; set; }
        public string ContactSurname { get; set; }
        public string ContactPhoneCode { get; set; }
        public string ContactPhoneNo { get; set; }
        public string ContactFaxCode { get; set; }
        public string ContactFaxNo { get; set; }
        public string AdditionalClientReference { get; set; }
        public string SubjectPhoneCode { get; set; }
        public string SubjectPhoneNo { get; set; }
        public string SubjectNameOnTicket { get; set; }
        public string SubjectAddress { get; set; }
        public string SubjectSuburb { get; set; }
        public string SubjectCity { get; set; }
        public string SubjectPostCode { get; set; }
        public string InvestigateOption { get; set; }
        public string BankAccountNo { get; set; }
        public string BankAbbreviation { get; set; }
        public string BankBranch { get; set; }
        public string BankBranchCode { get; set; }
        public string SpecialInstructions { get; set; }
        public string BankCreditAmount { get; set; }
        public string BankTermsGiven { get; set; }
        public string BankAccountHolder { get; set; }
        public string IsExistingClient { get; set; }
    }
}