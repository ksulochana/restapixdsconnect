﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace XDSConnectAPI.Models
{
    public enum BusinessSearchType { NAME, REGNO, /*ITNO, DUNSNO,*/ VATNO/*, BANKACCOUNTNO, TRADINGNO*/ };
    public enum BusinessResponseStatus { SUCCESS, FAILURE };
    public class BusinessSearchRequest
    {
       
        private string searchtypeField;

        private string subjectnameField;

        private string registrationnoField;
      
        private string vatnumberField;

        private string bankaccountnumberField;
        
        private string userField;

        private string passwordField;

        private string clientreferenceField;

        

        /// <remarks/>
        public string SearchType
        {
            get
            {
                return this.searchtypeField;
            }
            set
            {
                this.searchtypeField = value;
            }
        }

        /// <remarks/>
        public string SubjectName
        {
            get
            {
                return this.subjectnameField;
            }
            set
            {
                this.subjectnameField = value;
            }
        }

        /// <remarks/>
        public string RegistrationNo
        {
            get
            {
                return this.registrationnoField;
            }
            set
            {
                this.registrationnoField = value;
            }
        }

        /// <remarks/>
        //public string ITNumber
        //{
        //    get
        //    {
        //        return this.itnumberField;
        //    }
        //    set
        //    {
        //        this.itnumberField = value;
        //    }
        //}

        ///// <remarks/>
        //public string DunsNumber
        //{
        //    get
        //    {
        //        return this.dunsnumberField;
        //    }
        //    set
        //    {
        //        this.dunsnumberField = value;
        //    }
        //}

        /// <remarks/>
        public string VatNumber
        {
            get
            {
                return this.vatnumberField;
            }
            set
            {
                this.vatnumberField = value;
            }
        }

        /// <remarks/>
        public string BankAccountNumber
        {
            get
            {
                return this.bankaccountnumberField;
            }
            set
            {
                this.bankaccountnumberField = value;
            }
        }

        /// <remarks/>
        //public string TradingNumber
        //{
        //    get
        //    {
        //        return this.tradingnumberField;
        //    }
        //    set
        //    {
        //        this.tradingnumberField = value;
        //    }
        //}

        /// <remarks/>
        public string User
        {
            get
            {
                return this.userField;
            }
            set
            {
                this.userField = value;
            }
        }

        /// <remarks/>
        public string Password
        {
            get
            {
                return this.passwordField;
            }
            set
            {
                this.passwordField = value;
            }
        }

        /// <remarks/>
        public string ClientReference
        {
            get
            {
                return this.clientreferenceField;
            }
            set
            {
                this.clientreferenceField = value;
            }
        }

    }
}