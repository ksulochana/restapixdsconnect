﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace XDSConnectAPI.Models
{
    public class ValidationError
    {
        public string Field { get; set; }
        public string Message { get; set; }
    }
}