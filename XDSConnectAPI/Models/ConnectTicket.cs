﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace XDSConnectAPI.Models
{
    public class ConnectTicket
    {
        private string valueField;

        public string Value { get { return this.valueField; } set { this.valueField = value; } }
        public string Error { get; set; }
    }
}