﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace XDSConnectAPI.Models
{
   
    public class AMInput
    {
       public int ProductIndicator { get; set; }
       public string Company { get; set; }
       public string RuleSet { get; set; }
       public string Branch { get; set; }
       public string User { get; set; }
       public string Password { get; set; }
       public string IsExistingClient { get; set; }
       public string GrossMonthlyIncome { get; set; }
       public string Surname { get; set; }
       public string FirstName { get; set; }
       public string IDType { get; set; }
       public string BirthDate { get; set; }
       public string IdentityNumber { get; set; }
       public string Gender { get; set; }
       public string MaritalStatus { get; set; }
       public string AddressLine1 { get; set; }
       public string AddressLine2 { get; set; }
       public string Suburb { get; set; }
       public string City { get; set; }
       public string PostalCode { get; set; }
       public string HomeTelephoneCode { get; set; }
       public string HomeTelephoneNumber { get; set; }
       public string WorkTelephoneCode { get; set; }
       public string WorkTelephoneNumber { get; set; }
       public string CellNumber { get; set; }
       public string Occupation { get; set; }
       public string Employer { get; set; }
       public string NumberOfYearsAtEmployer { get; set; }
       public string BankName { get; set; }
       public string BankAccountType { get; set; }
       public string BankBranchCode { get; set; }
       public string BankAccountNumber { get; set; }
       public string HighestQualification { get; set; }
      
    }
}