﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
//using System.Web.Mvc;
using System.Net.Http;
using XDSConnectAPI.Models;
//using ConnectModels;
using System.Reflection;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Configuration;
using System.Web.Script.Serialization;
using System.Data.SqlClient;
using System.Data;
using System.Net;

namespace XDSConnectAPI.Controllers
{
    public class SubmitApplicationController : ApiController
    {        
        //
        // GET: /SubmitApplication/
        //[HttpPost]
        public MTNOutput/*AOConnect.MTNOutput*/ Post(MTNInput oInput)
        {
            string step = "0";
            string Logcon = ConfigurationManager.AppSettings["LogCon"];
            DateTime dtStartdate = DateTime.Now;
            MTNOutput mTNOutput = new MTNOutput();

          //  XDSPortalLibrary.Entity_Layer.MTNHResponse.MTNOutput mTNOutput = new XDSPortalLibrary.Entity_Layer.MTNHResponse.MTNOutput();
            try
            {

                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
                AOConnect.MTNInput oAOInput = new AOConnect.MTNInput();
                step = "1";
                oAOInput.AddressLine1 = oInput.AddressLine1;
                oAOInput.AddressLine2 = oInput.AddressLine2;
                oAOInput.BankAccountNumber = oInput.BankAccountNumber;
                oAOInput.BankAccountType = oInput.BankAccountType;
                oAOInput.BankBranchCode = oInput.BankBranchCode;
                oAOInput.BankName = oInput.BankName;
                oAOInput.BirthDate = oInput.BirthDate;
                oAOInput.Branch = oInput.Branch;
                oAOInput.CellNumber = oInput.CellNumber;
                oAOInput.City = oInput.City;
                oAOInput.Company = oInput.Company;
                oAOInput.Employer = oInput.Employer;
                oAOInput.FirstName = oInput.FirstName;
                oAOInput.Gender = oInput.Gender;
                oAOInput.GrossMonthlyIncome = oInput.GrossMonthlyIncome;
                oAOInput.HighestQualification = oInput.HighestQualification;
                oAOInput.HomeTelephoneCode = oInput.HomeTelephoneCode;
                oAOInput.HomeTelephoneNumber = oInput.HomeTelephoneNumber;
                oAOInput.IdentityNumber = oInput.IdentityNumber;
                step = "2";
                oAOInput.IDType = oInput.IDType;
                oAOInput.IsExistingClient = oInput.IsExistingClient;
                oAOInput.MaritalStatus = oInput.MaritalStatus;
                oAOInput.NumberOfYearsAtEmployer = oInput.NumberOfYearsAtEmployer;
                oAOInput.Occupation = oInput.Occupation;
                oAOInput.Password = oInput.Password;
                oAOInput.PostalCode = oInput.PostalCode;
                oAOInput.RuleSet = oInput.RuleSet;
                oAOInput.Suburb = oInput.Suburb;
                oAOInput.Surname = oInput.Surname;
                oAOInput.User = oInput.User;
                oAOInput.WorkTelephoneCode = oInput.WorkTelephoneCode;
                oAOInput.WorkTelephoneNumber = oInput.WorkTelephoneNumber;
                step = "3";
                oAOInput.DealerCode = oInput.DealerCode;
                oAOInput.CustomerName = oInput.CustomerName;
                oAOInput.Channel = oInput.Channel;
                oAOInput.AlternateContactNumber = oInput.AlternateContactNumber;
                oAOInput.EmailAddress = oInput.EmailAddress;

                AOConnect.MHConnect oMhconnect = new AOConnect.MHConnect();
                step = "4";
                AOConnect.MTNOutput oMTNOutput  //=new AOConnect.MTNOutput();//
                = oMhconnect.SubmitApplicationATelesales(oAOInput);
                step = "5";
                mTNOutput = (MTNOutput)XDSConnectAPI.Services.ObjectCopier.Copy(oMTNOutput, mTNOutput, null, null);
            }
            catch (Exception ex)
            {
                File.AppendAllText(@"C:\Log\XDSConnectAPI.txt", step+"-"+DateTime.Now.ToString()+":"+ex.Message+"\n");
                if (mTNOutput == null)
                {
                    mTNOutput = new MTNOutput();
                }
                if (mTNOutput.BureauResponse == null)
                {
                    mTNOutput.BureauResponse = new BureauResponse();
                }
                DateTime dtenddate = DateTime.Now;
                double diff = dtenddate.Subtract(dtStartdate).TotalSeconds;

                mTNOutput.BureauResponse.ProcessingTimeSecs = Math.Round(float.Parse(diff.ToString()), 2);

                mTNOutput.Status = MTNOutput.enumARstatus.FAILURE;
                mTNOutput.ErrorDescription = "Error While Processing your request, Please Contact XDS";

                mTNOutput.Outcome = MTNOutput.enumARoutcome.DECLINE;
                mTNOutput.BureauResponse.ResponseStatus = BureauResponse.enumresponseStatus.FAILURE;
                mTNOutput.Reason = "DECLINE";
                mTNOutput.BureauResponse.UniqueRefGuid = (Guid.NewGuid()).ToString();
            }

            try
            {
                //  File.AppendAllText(@"C:\Log\Response.txt", "Enterdeserialization");
                var response = new JavaScriptSerializer().Serialize(mTNOutput);

                SqlConnection con = new SqlConnection(Logcon);

                SqlCommand cmd = new SqlCommand("Insert into CustomVettingQAResponse (CreateByUser,Response,CreatedOnDate) values(@ID, @inputXMLstring,getdate())", con);
                cmd.Parameters.AddWithValue("@ID", mTNOutput.BureauResponse.UniqueRefGuid);
                cmd.Parameters.AddWithValue("@inputXMLstring", response);
                if (con.State == ConnectionState.Closed)
                    con.Open();

                cmd.ExecuteNonQuery();

                con.Close();
            }

            catch (Exception ex)
            {
                File.AppendAllText(@"C:\Log\XDSConnectAPI.txt", DateTime.Now.ToString() + ":2:" + ex.Message + "\n");
                SqlConnection con = new SqlConnection(Logcon);

                SqlCommand cmd = new SqlCommand("Insert into CustomVettingQAResponse (CreateByUser,Response,CreatedOnDate) values(@ID, @inputXMLstring,getdate())", con);
                cmd.Parameters.AddWithValue("@ID", mTNOutput.BureauResponse.UniqueRefGuid);
                cmd.Parameters.AddWithValue("@inputXMLstring", ex.Message);
                if (con.State == ConnectionState.Closed)
                    con.Open();

                cmd.ExecuteNonQuery();

                con.Close();

            }

            return mTNOutput;
        }

        //private Object CreateObject(string XMLString, Object YourClassObject)
        //{
        //    if (XMLString.IndexOf("NoResult") >= 0)
        //    {
        //        //e.g. <NoResult><Error>Invalid IDno Supplied</Error></NoResult>
        //        //     <NoResult><NotFound>No Record Found</NotFound></NoResult>
                

        //        XmlDocument xml = new XmlDocument();
        //        xml.LoadXml(XMLString);
        //        switch (xml.DocumentElement.FirstChild.Name)
        //        {
        //            case "Error":
        //            case "NotFound":
        //               // _errorMessage = xml.DocumentElement.FirstChild.InnerText;
        //                break;
        //            case "Validate":// _hasError = false;
        //                // _errorMessage = xml.DocumentElement.FirstChild.InnerText;
        //                break;
        //            default:
        //                //todo: log error - not expected.
        //               // _errorMessage = string.Format("No result ({0})", xml.DocumentElement.FirstChild.Name);
        //                break;
        //        }
        //        xml = null;
        //        YourClassObject = null;
        //    }
        //    else
        //    {

        //        try
        //        {
        //            XmlSerializer oXmlSerializer = new XmlSerializer(YourClassObject.GetType());
        //            //The StringReader will be the stream holder for the existing XML file 
        //            YourClassObject = oXmlSerializer.Deserialize(new XmlTextReader(new StringReader(XMLString)));
        //            //initially deserialized, the data is represented by an object without a defined type 
        //        }
        //        catch (Exception ex)
        //        {

        //            throw ex;
        //        }


        //    }

        //    return YourClassObject;
        //}
      
    }
}