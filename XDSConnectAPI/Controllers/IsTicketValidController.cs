﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace XDSConnectAPI.Controllers
{
    public class IsTicketValidController : ApiController
    {
        // GET: api/IsTicketValid/5
        public bool Get(string ConnectTicket)
        {
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
            XDSConnect.XDSConnectWS xdsConnect = new XDSConnect.XDSConnectWS();
            bool Result = xdsConnect.IsTicketValid(ConnectTicket);

            return Result;
        }
    }
}
