﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using XDSConnectAPI.Models.MTNSOAResponse;
using System.Configuration;
using System.Xml.Serialization;
using System.Data.SqlClient;
using System.Web.Script.Serialization;
using System.Data;
using System.Net;

namespace XDSConnectAPI.Controllers
{
    public class ConnectCustomVettingQController : ApiController
    {
        //
        // GET: /ConnectCustomVettingQ/
        public SubmitResponse Post(XDSConnectAPI.Models.AMInput oInput)
        {

            string Logcon = ConfigurationManager.AppSettings["LogCon"];
            DateTime dtStartdate = DateTime.Now;
           

            SubmitResponse submitResponse = new SubmitResponse();
            try
            {
                //try
                //{
                //    File.AppendAllText(@"C:\Log\XDSConnectAPI.txt", oInput.IdentityNumber + "-" + DateTime.Now.ToString() + ":" + "\n");
                //}
                //catch (Exception ex)
                //{

                //}
                //  string RuleSet= string.Empty; string Branch= string.Empty; string User= string.Empty; string Password= string.Empty; string IsExistingClient= string.Empty; string GrossMonthlyIncome= string.Empty; 
                // string Surname= string.Empty; string FirstName= string.Empty; string IDType= string.Empty; string BirthDate= string.Empty; string IdentityNumber= string.Empty; string Gender= string.Empty; string MaritalStatus= string.Empty; string AddressLine1= string.Empty; 
                // string AddressLine2= string.Empty; string Suburb= string.Empty; string City= string.Empty; string PostalCode= string.Empty; string HomeTelephoneCode= string.Empty; string HomeTelephoneNumber= string.Empty; string WorkTelephoneCode= string.Empty; string WorkTelephoneNumber= string.Empty; string CellNumber= string.Empty; string Occupation= string.Empty; string Employer= string.Empty; 
                //string NumberOfYearsAtEmployer= string.Empty; string BankName= string.Empty; string BankAccountType= string.Empty; string BankBranchCode= string.Empty; string BankAccountNumber= string.Empty; string HighestQualification= string.Empty;
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
                AMSOAConnect.MTNConnect oMtnconnect = new AMSOAConnect.MTNConnect();
                //try
                //{
                //    File.AppendAllText(@"C:\Log\XDSConnectAPI2.txt", oInput.IdentityNumber + "-" + DateTime.Now.ToString() + ":" + "\n");
                //}
                //catch (Exception ex)
                //{

                //}
                AMSOAConnect.SubmitResponse osubmitResponse = new AMSOAConnect.SubmitResponse();

                //try
                //{
                //    File.AppendAllText(@"C:\Log\XDSConnectAPI1.txt", oInput.IdentityNumber + "-" + DateTime.Now.ToString() + ":" +"step 2"+ "\n");
                //}
                //catch (Exception ex)
                //{

                //}
                osubmitResponse = oMtnconnect.ConnectCustomVettingQA(oInput.ProductIndicator, oInput.Company, oInput.RuleSet, oInput.Branch, oInput.User, oInput.Password, oInput.IsExistingClient, oInput.GrossMonthlyIncome, oInput.Surname, oInput.FirstName, oInput.IDType, oInput.BirthDate, oInput.IdentityNumber, oInput.Gender, oInput.MaritalStatus, oInput.AddressLine1, oInput.AddressLine2, oInput.Suburb, oInput.City, oInput.PostalCode, oInput.HomeTelephoneCode, oInput.HomeTelephoneNumber, oInput.WorkTelephoneCode, oInput.WorkTelephoneNumber, oInput.CellNumber, oInput.Occupation, oInput.Employer, oInput.NumberOfYearsAtEmployer, oInput.BankName, oInput.BankAccountType, oInput.BankBranchCode, oInput.BankAccountNumber, oInput.HighestQualification);

                submitResponse = (SubmitResponse)XDSConnectAPI.Services.ObjectCopier.Copy(osubmitResponse, submitResponse, null, null);
            }
            catch (Exception ex)
            {
                File.AppendAllText(@"C:\Log\XDSConnectAPICustomVetting.txt", DateTime.Now.ToString() + ":" + ex.Message + "\n");
                if (submitResponse.SubmitResult == null)
                {
                    submitResponse.SubmitResult = new SubmitResult();
                }
                if (submitResponse.SubmitResult.BureauResponse == null)
                {
                    submitResponse.SubmitResult.BureauResponse = new BureauResponse();
                }
                DateTime dtenddate = DateTime.Now;
                double diff = dtenddate.Subtract(dtStartdate).TotalSeconds;

                submitResponse.SubmitResult.BureauResponse.ProcessingTimeSecs = Math.Round(float.Parse(diff.ToString()), 2);

                submitResponse.SubmitResult.Status = SubmitResult.enumARstatus.FAILURE;
                submitResponse.SubmitResult.ErrorDescription = "Error While Processing your request, Please Contact XDS";

                submitResponse.SubmitResult.Outcome = SubmitResult.enumARoutcome.DECLINE;
                submitResponse.SubmitResult.BureauResponse.ResponseStatus = ResponseStatus.FAILURE;
                submitResponse.SubmitResult.Reason = "DECLINE";
                submitResponse.SubmitResult.BureauResponse.UniqueRefGuid = (Guid.NewGuid());
            }

            try
            {
                //  File.AppendAllText(@"C:\Log\Response.txt", "Enterdeserialization");
                var response = new JavaScriptSerializer().Serialize(submitResponse);

                SqlConnection con = new SqlConnection(Logcon);

                SqlCommand cmd = new SqlCommand("Insert into CustomVettingQAResponse (CreateByUser,Response,CreatedOnDate) values(@ID, @inputXMLstring,getdate())", con);
                cmd.Parameters.AddWithValue("@ID", submitResponse.SubmitResult.BureauResponse.UniqueRefGuid);
                cmd.Parameters.AddWithValue("@inputXMLstring", response);
                if (con.State == ConnectionState.Closed)
                    con.Open();

                cmd.ExecuteNonQuery();

                con.Close();
            }

            catch (Exception ex)
            {
                File.AppendAllText(@"C:\Log\XDSConnectAPICustomVetting.txt", DateTime.Now.ToString() + ":2:" + ex.Message + "\n");
                SqlConnection con = new SqlConnection(Logcon);

                SqlCommand cmd = new SqlCommand("Insert into CustomVettingQAResponse (CreateByUser,Response,CreatedOnDate) values(@ID, @inputXMLstring,getdate())", con);
                cmd.Parameters.AddWithValue("@ID", submitResponse.SubmitResult.BureauResponse.UniqueRefGuid);
                cmd.Parameters.AddWithValue("@inputXMLstring", ex.Message);
                if (con.State == ConnectionState.Closed)
                    con.Open();

                cmd.ExecuteNonQuery();

                con.Close();

            }
            return submitResponse;
        }
	}
}