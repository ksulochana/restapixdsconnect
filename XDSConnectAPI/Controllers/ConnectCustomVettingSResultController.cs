﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
//using System.Web.Mvc;
using System.Net.Http;
using XDSConnectAPI.Models;
//using ConnectModels;
using System.Reflection;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Configuration;
using System.Web.Script.Serialization;
using System.Data.SqlClient;
using System.Data;
using XDSConnectAPI.Models.Commercial;
using System.Net;

namespace XDSConnectAPI.Controllers
{
    public class ConnectCustomVettingSResultController:ApiController
    {
        public  MTNCommercialFinalOutput  Post(RegEntityResultInput oInput)
        {
            string Logcon = ConfigurationManager.AppSettings["LogCon"];
            DateTime dtStartdate = DateTime.Now;
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
            ADPConnect.MTNCommercialFinalOutput oResponse = new ADPConnect.MTNCommercialFinalOutput();
            MTNCommercialFinalOutput olResponse = new MTNCommercialFinalOutput();

            if (olResponse == null)
            {
                olResponse = new MTNCommercialFinalOutput();
            }
            if (olResponse.BureauData == null)
            {
                olResponse.BureauData = new CommercialResponse();
                olResponse.BureauData.processingStartDate = dtStartdate;
                olResponse.BureauData.uniqueRefGuid = Guid.NewGuid().ToString();
            }

            //  XDSPortalLibrary.Entity_Layer.MTNHResponse.MTNOutput mTNOutput = new XDSPortalLibrary.Entity_Layer.MTNHResponse.MTNOutput();
            try
            {
                ADPConnect.ADPConnect oAdpconnect = new ADPConnect.ADPConnect();

                oResponse
                = oAdpconnect.ConnectCustomVettingSResultA(oInput.ProductCode, oInput.User, oInput.Password, oInput.BureauNumber, oInput.EnquiryReason, 
                oInput.EnquiryAmount, oInput.Terms, oInput.UserOnTicket, oInput.ContactForename, oInput.ContactSurname, oInput.ContactPhoneCode,
                oInput.ContactPhoneNo,oInput.ContactFaxCode,oInput.ContactFaxNo, oInput.AdditionalClientReference,oInput.SubjectPhoneCode,oInput.SubjectPhoneNo,
                oInput.SubjectNameOnTicket,oInput.SubjectAddress,oInput.SubjectSuburb,oInput.SubjectCity,oInput.SubjectPostCode,oInput.InvestigateOption,
                oInput.BankAccountNo,oInput.BankAbbreviation,oInput.BankBranch,oInput.BankBranchCode,oInput.SpecialInstructions,oInput.BankCreditAmount,
                oInput.BankTermsGiven,oInput.BankAccountHolder,oInput.IsExistingClient);

                olResponse = (MTNCommercialFinalOutput)XDSConnectAPI.Services.ObjectCopier.Copy(oResponse, olResponse, null, null);
            }
            catch (Exception ex)
            {
                File.AppendAllText(@"C:\Log\XDSConnectAPI.txt", DateTime.Now.ToString() + ":" + ex.Message + "\n");
                if (olResponse == null)
                {
                    olResponse = new MTNCommercialFinalOutput();
                }
                if (olResponse.BureauData == null)
                {
                    olResponse.BureauData = new CommercialResponse();
                }
                DateTime dtenddate = DateTime.Now;
                double diff = dtenddate.Subtract(dtStartdate).TotalSeconds;

                olResponse.BureauData.processingTimeSecs = float.Parse(diff.ToString());

                olResponse.BureauData.responseStatus = ResponseStatus.Failure;
                olResponse.BureauData.errorMessage = "Error While Processing your request, Please Contact XDS";

            }

            try
            {
                //  File.AppendAllText(@"C:\Log\Response.txt", "Enterdeserialization");
                var response = new JavaScriptSerializer().Serialize(olResponse);

                SqlConnection con = new SqlConnection(Logcon);

                SqlCommand cmd = new SqlCommand("Insert into CustomVettingSReportAResponse (CreateByUser,Response,CreatedOnDate) values(@ID, @inputXMLstring,getdate())", con);
                cmd.Parameters.AddWithValue("@ID", olResponse.BureauData.uniqueRefGuid);
                cmd.Parameters.AddWithValue("@inputXMLstring", response);
                if (con.State == ConnectionState.Closed)
                    con.Open();

                cmd.ExecuteNonQuery();

                con.Close();
            }

            catch (Exception ex)
            {
                File.AppendAllText(@"C:\Log\XDSConnectAPI.txt", DateTime.Now.ToString() + ":2:" + ex.Message + "\n");
                SqlConnection con = new SqlConnection(Logcon);

                SqlCommand cmd = new SqlCommand("Insert into CustomVettingSReportAResponse (CreateByUser,Response,CreatedOnDate) values(@ID, @inputXMLstring,getdate())", con);
                cmd.Parameters.AddWithValue("@ID", olResponse.BureauData.uniqueRefGuid);
                cmd.Parameters.AddWithValue("@inputXMLstring", ex.Message);
                if (con.State == ConnectionState.Closed)
                    con.Open();

                cmd.ExecuteNonQuery();

                con.Close();

            }

            return olResponse;
        }
    }
}