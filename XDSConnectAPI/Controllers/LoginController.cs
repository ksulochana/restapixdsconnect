﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using XDSConnectAPI.Models;

namespace XDSConnectAPI.Controllers
{
    public class LoginController : ApiController
    {
        // GET: api/Login/5/2
        public ConnectTicket Get(string Username, string Password)
        {
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
            XDSConnect.XDSConnectWS xdsConnect = new XDSConnect.XDSConnectWS();
            string Result = xdsConnect.Login(Username, Password);

            ConnectTicket connectTicket = new ConnectTicket();

            if (Result == "UserNotFound" || Result == "NotAuthenticated")
            {
                connectTicket.Error = Result;
            }
            else
            {
                connectTicket.Value = Result;
            }

            return connectTicket;
        }
    }
}
