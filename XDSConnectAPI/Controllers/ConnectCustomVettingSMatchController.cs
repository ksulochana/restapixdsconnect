﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
//using System.Web.Mvc;
using System.Net.Http;
using XDSConnectAPI.Models;
//using ConnectModels;
using System.Reflection;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Configuration;
using System.Web.Script.Serialization;
using System.Data.SqlClient;
using System.Data;
using System.Net;

namespace XDSConnectAPI.Controllers
{
    public class ConnectCustomVettingSMatchController : ApiController
    {        
        //
        // GET: /SubmitApplication/
        //[HttpPost]
        public BusinessSearchResponse/*AOConnect.MTNOutput*/ Post(BusinessSearchRequest oInput)
        {
            string Logcon = ConfigurationManager.AppSettings["LogCon"];
            DateTime dtStartdate = DateTime.Now;
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
            ADPConnect.BusinessSearchResponse oResponse = new ADPConnect.BusinessSearchResponse();
            BusinessSearchResponse olResponse = new BusinessSearchResponse();

            if (olResponse == null)
            {
                olResponse = new BusinessSearchResponse();
            }
            if (olResponse.BusinessSearchResult == null)
            {
                olResponse.BusinessSearchResult = new BusinessSearchResult();
                olResponse.BusinessSearchResult.ProcessingStartDate = dtStartdate;
                olResponse.BusinessSearchResult.UniqueRefGuid = Guid.NewGuid().ToString();
            }

            //  XDSPortalLibrary.Entity_Layer.MTNHResponse.MTNOutput mTNOutput = new XDSPortalLibrary.Entity_Layer.MTNHResponse.MTNOutput();
            try
            {

                ADPConnect.ADPConnect oMtnconnect = new ADPConnect.ADPConnect();

                oResponse
                = oMtnconnect.ConnectCustomVettingSMatchA(oInput.SearchType,oInput.SubjectName,oInput.RegistrationNo, oInput.VatNumber,oInput.BankAccountNumber, oInput.User,oInput.Password,oInput.ClientReference);

                


                olResponse = (BusinessSearchResponse)XDSConnectAPI.Services.ObjectCopier.Copy(oResponse, olResponse, null, null);
            }
            catch (Exception ex)
            {
                File.AppendAllText(@"C:\Log\XDSConnectAPI.txt",DateTime.Now.ToString()+":"+ex.Message+"\n");
                if (olResponse == null)
                {
                    olResponse = new BusinessSearchResponse();
                }
                if (olResponse.BusinessSearchResult == null)
                {
                    olResponse.BusinessSearchResult = new BusinessSearchResult();
                }
                DateTime dtenddate = DateTime.Now;
                double diff = dtenddate.Subtract(dtStartdate).TotalSeconds;

                olResponse.BusinessSearchResult.ProcessingTimeSecs =float.Parse(diff.ToString());

                olResponse.BusinessSearchResult.ResponseStatus = BusinessResponseStatus.FAILURE;
                olResponse.BusinessSearchResult.ErrorDescription = "Error While Processing your request, Please Contact XDS";
               
            }

            try
            {
                //  File.AppendAllText(@"C:\Log\Response.txt", "Enterdeserialization");
                var response = new JavaScriptSerializer().Serialize(olResponse);

                SqlConnection con = new SqlConnection(Logcon);

                SqlCommand cmd = new SqlCommand("Insert into CustomVettingSAResponse (CreateByUser,Response,CreatedOnDate) values(@ID, @inputXMLstring,getdate())", con);
                cmd.Parameters.AddWithValue("@ID", olResponse.BusinessSearchResult.UniqueRefGuid);
                cmd.Parameters.AddWithValue("@inputXMLstring", response);
                if (con.State == ConnectionState.Closed)
                    con.Open();

                cmd.ExecuteNonQuery();

                con.Close();
            }

            catch (Exception ex)
            {
                File.AppendAllText(@"C:\Log\XDSConnectAPI.txt", DateTime.Now.ToString() + ":2:" + ex.Message + "\n");
                SqlConnection con = new SqlConnection(Logcon);

                SqlCommand cmd = new SqlCommand("Insert into CustomVettingSAResponse (CreateByUser,Response,CreatedOnDate) values(@ID, @inputXMLstring,getdate())", con);
                cmd.Parameters.AddWithValue("@ID", olResponse.BusinessSearchResult.UniqueRefGuid);
                cmd.Parameters.AddWithValue("@inputXMLstring", ex.Message);
                if (con.State == ConnectionState.Closed)
                    con.Open();

                cmd.ExecuteNonQuery();

                con.Close();

            }
           
            return olResponse;
        }

      
      
    }
}