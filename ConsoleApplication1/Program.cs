﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Net;
//using Newtonsoft.Json.Schema;
using NJsonSchema;

namespace ConsoleApplication1
{
    class Program
    {
      //  private const string URL = "http://www.uat.xds.co.za/XDSConnectAPI";
        private string urlParameters = "?Username=ksulochana&Password=Sulo@1987";

        private const string URL = "http://localhost:7428/api/IsTicketValid";
        //private string urlParameters = "?ConnectTicket={ksulochana}";

        static void Main(string[] args)
        {
            //Newtonsoft.Json.Schema.JsonSchemaGenerator oGen = new Newtonsoft.Json.Schema.JsonSchemaGenerator();

            //Newtonsoft.Json.Schema.JsonSchema jschema = new Newtonsoft.Json.Schema.JsonSchema();

            //jschema = oGen.Generate(typeof(XDSConnectAPI.Models.MTNOutput));

            var schemaFromType = JsonSchema.FromType<XDSConnectAPI.Models.MTNOutput>();
            var json = schemaFromType.ToJson();
            Console.WriteLine(schemaFromType.ToJson());

            //XDSConnectAPI.Models.MTNOutput mTNOutput = new XDSConnectAPI.Models.MTNOutput();

            //foreach (var prop in mTNOutput.GetType().GetProperties())
            //{
            //    var type = Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType;
            //}

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(URL);



            //// Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));

            //// List data response.
            //var response = client.GetStringAsync(urlParameters).Result;  // Blocking call!



            MTNInput oInput = new MTNInput();
            // AMInput oInput = new AMInput();
            oInput.AddressLine1 = "";
            oInput.AddressLine2 = "";
            oInput.Suburb = "";
            oInput.BankAccountNumber = "";
            oInput.BankAccountType = "";
            oInput.BankBranchCode = "";
            oInput.BankName = "";
            oInput.BirthDate = "1998-05-13T00:00:00Z";
            oInput.User = "ksulochana";// "ADP_UAT";
            oInput.Password = "cal100";// "Sulo@1987";// "053pfr9t";
            oInput.IDType = "SAID";
            oInput.IdentityNumber = "9805135668088";
            oInput.Surname = "DAYENI";
            oInput.FirstName = "MBULELO ANDREW";
            oInput.GrossMonthlyIncome = "3769";
            oInput.Gender = "M";
             oInput.PostalCode = "";
            oInput.HomeTelephoneCode = "";
            oInput.Employer = "";
            oInput.City = "";
            oInput.WorkTelephoneCode = "";
            oInput.MaritalStatus = "";
            oInput.Occupation = "";
            //  oInput.NumberOfYearsAtEmployer = "1";
            oInput.WorkTelephoneNumber = "";
            oInput.CellNumber = "";
            oInput.HighestQualification = "";
            oInput.DealerCode = "SABC0046";
            oInput.CustomerName = "MBULELO ANDREW";
            oInput.IsExistingClient = "false";

            AMInput oInput1 = new AMInput();
            // AMInput oInput = new AMInput();
            oInput1.ProductIndicator = 1;
            oInput1.Company = "MTN";
            oInput1.RuleSet = "MTN";
            oInput1.Branch = "online";
            oInput1.AddressLine1 = "MALAMULELE";
            oInput1.AddressLine2 = "MALAMULELE";
            oInput1.Suburb = "";
            oInput1.BankAccountNumber = "1222133553";
            oInput1.BankAccountType = "SAVINGSACCOUNT";
            oInput1.BankBranchCode = "470010";
            oInput1.BankName = "CAPITEC";
            oInput1.BirthDate = "1982-12-28T00:00:00Z";
            oInput1.User = "ksulochana";
            oInput1.Password = "cal100";
            oInput1.IDType = "SAID";
            oInput1.IdentityNumber = "5808235313088";
            oInput1.Surname = "Malule'ke";
            oInput1.FirstName = "Hasani  Carling";
            oInput1.GrossMonthlyIncome = "4500";
            oInput1.Gender = "M";
            oInput1.IsExistingClient = "F";
            oInput1.PostalCode = "982";
            oInput1.HomeTelephoneCode = "27";
            oInput1.Employer = "";
            oInput1.City = "THOHOYANDOU";
            oInput1.WorkTelephoneCode = "MaritalStatus";
            oInput1.MaritalStatus = "Single";
            oInput1.Occupation = "SELF-EMPLOYED";
            oInput1.NumberOfYearsAtEmployer = "10";
            oInput1.WorkTelephoneNumber = "787944734";
            oInput1.CellNumber = "27787944734";
            oInput1.HighestQualification = "";
            oInput1.IsExistingClient = "true";
            oInput1.HomeTelephoneNumber = "787944734";


            string toBeSend = JsonConvert.SerializeObject(oInput);

            MTNInput oInput2 = new MTNInput();
            // AMInput oInput = new AMInput();
            oInput2.AddressLine1 = "WELBEDACHT WEST";
            oInput2.AddressLine2 = "WELBEDACHT WEST";
            oInput2.Suburb = "";
            oInput2.BankAccountNumber = "1535055721";
            oInput2.BankAccountType = "SAVINGSACCOUNT";
            oInput2.BankBranchCode = "470010";
            oInput2.BankName = "Bidvest Bank";
            oInput2.BirthDate = "990-12-06T00:00:00Z";
            oInput2.User = "ksulochana";
            oInput2.Password = "cal100";
            oInput2.IDType = "1";
            oInput2.IdentityNumber = "9903155200088";// "5707190151089";
            oInput2.Surname = "MSEBELE";// "PETERSON";
            oInput2.FirstName = "AWAKHIWE";// "ELIZABETH";

            //oInput2.IdentityNumber = "5707190151089";
            //oInput2.Surname = "PETERSON";
            //oInput2.FirstName =  "ELIZABETH";

            oInput2.GrossMonthlyIncome = "41991";
            oInput2.Gender = "F";
            oInput2.BirthDate = "19761205";
            oInput2.IsExistingClient = "true";
            oInput2.PostalCode = "4092";
            oInput2.HomeTelephoneCode = "27";
            oInput2.Employer = "Thia";
            oInput2.City = "CHATSWORTH";
            oInput2.WorkTelephoneCode = "";
            oInput2.MaritalStatus = "Single";
             oInput2.Occupation = "SELF-EMPLOYED";
             oInput2.NumberOfYearsAtEmployer = "1";
            oInput2.WorkTelephoneNumber = "3146894";
            oInput2.CellNumber = "27635156498";
            oInput2.HighestQualification = "";
            oInput2.IsExistingClient = "F";
            oInput2.Branch = "Online";
            oInput2.Channel = "Store";
            oInput2.Company = "MTN";
            oInput2.RuleSet = "MTN";
            oInput2.DealerCode = "SABC1006";
            oInput2.EmailAddress = "635156498@noemail.co.za";
            oInput2.HomeTelephoneCode = "";
            oInput2.HomeTelephoneNumber = "635156498";
            oInput2.AlternateContactNumber = "";
            oInput2.CustomerName = "Nelisiwe Nomalizo";

            


            using (WebClient client1 = new WebClient())
            {
                client1.Headers[HttpRequestHeader.ContentType] = "application/json";
                var jsonObj = JsonConvert.SerializeObject(oInput);
                var jsonObj1 = JsonConvert.SerializeObject(oInput1);
                var jsonObj2 = JsonConvert.SerializeObject(oInput2);
                // var dataString = client1.UploadString("http://localhost:7428/api/SubmitApplication", jsonObj2);
              //  var dataString = client1.UploadString("https://www.uat.xds.co.za/XDSConnectAPI2/api/SubmitApplication", jsonObj2);
               //var dataString = client1.UploadString("https://www.web.xds.co.za/RXConnect/api/SubmitApplication", jsonObj2);
              //  var dataString = client1.UploadString("https://www.web.xds.co.za/RXConnect/api/ConnectCustomVettingQ", jsonObj1);
                  var dataString = client1.UploadString("https://www.web.xds.co.za/RXConnect/api/SubmitApplication", jsonObj2);
                // var data1 = JsonConvert.DeserializeObject<OutputData>(dataString);
                //  Console.WriteLine(JsonConvert.SerializeObject(data1));
              //  var dataString = client1.UploadString("https://www.uat.xds.co.za/XDSConnectAPItest/api/ConnectCustomVettingQ", jsonObj1);


                Console.ReadLine();


            }

                //RegEntityMatchInput oI = new RegEntityMatchInput();
                //oI.SearchType = "REGNO";
                //oI.SubjectName = "Grinmark Realtors Cc";
                //oI.RegistrationNo = "199404164523";
                //oI.User = "ksulochana";
                //oI.Password = "Sulo@1987";
                //oI.ClientReference = string.Empty;
                //oI.BankAccountNumber = string.Empty;
                //oI.DunsNumber = string.Empty;
                //oI.ITNumber = string.Empty;
                //oI.TradingNumber = string.Empty;
                //oI.VatNumber = string.Empty;

                //string toBeSend = JsonConvert.SerializeObject(oI);
                //using (WebClient client1 = new WebClient())
                //{
                //    client1.Headers[HttpRequestHeader.ContentType] = "application/json";
                //    var jsoncomObj = JsonConvert.SerializeObject(oI);
                //    var dataString = client1.UploadString("https://www.uat.xds.co.za/xdsconnectAPI/api/ConnectCustomVettingSMatch", jsoncomObj);
                //}


                //    var response = client.PostAsJsonAsync("api/ConnectCustomVettingSMatch", toBeSend).Result;

                //RegEntityResultInput oresult = new RegEntityResultInput();
                //oresult.ProductCode = 0;
                //oresult.User = "ksulochana";
                //oresult.Password = "Sulo@1987";
                //oresult.TUNumber = "56421679:58284467";
                //oresult.EnquiryReason = "Test";
                //oresult.EnquiryAmount = "0";
                //oresult.Terms = string.Empty;
                //oresult.UserOnTicket = string.Empty;
                //oresult.ContactForename = string.Empty;
                //oresult.ContactSurname = string.Empty;
                //oresult.ContactPhoneCode = string.Empty;
                //oresult.ContactPhoneNo = string.Empty;
                //oresult.ContactFaxCode = string.Empty;
                //oresult.ContactFaxNo = string.Empty;
                //oresult.AdditionalClientReference = string.Empty;
                //oresult.SubjectPhoneCode = string.Empty;
                //oresult.SubjectPhoneNo = string.Empty;
                //oresult.SubjectNameOnTicket = string.Empty;
                //oresult.SubjectAddress = string.Empty;
                //oresult.SubjectSuburb = string.Empty;
                //oresult.SubjectCity = string.Empty;
                //oresult.SubjectPostCode = string.Empty;
                //oresult.InvestigateOption = string.Empty;
                //oresult.BankAccountNo = string.Empty;
                //oresult.BankAbbreviation = string.Empty;
                //oresult.BankBranch = string.Empty;
                //oresult.BankBranchCode = string.Empty;
                //oresult.SpecialInstructions = string.Empty;
                //oresult.BankCreditAmount = string.Empty;
                //oresult.BankTermsGiven = string.Empty;
                //oresult.BankAccountHolder = string.Empty;
                //oresult.IsExistingClient = "3";

                //string toBeSend = JsonConvert.SerializeObject(oresult);
                //using (WebClient client1 = new WebClient())
                //{
                //    client1.Headers[HttpRequestHeader.ContentType] = "application/json";
                //    var jsoncomObj = JsonConvert.SerializeObject(oresult);
                //    var dataString = client1.UploadString("https://www.uat.xds.co.za/xdsconnectAPI/api/ConnectCustomVettingSResult", jsoncomObj);
                //}
                //var response = client.PostAsJsonAsync("api/ConnectCustomVettingSResult", toBeSend).Result;
                //  var releases = JArray.Parse(response);


            //    VettingVRequest nonRegEntityRequest = new VettingVRequest();

            //nonRegEntityRequest.User = "ksulochana";
            //nonRegEntityRequest.Password = "Sulo@1987";


            //PrincipalInfo principalInfo = new PrincipalInfo();
            //principalInfo.IDType = "1";
            //principalInfo.IdentityNumber = "8608301250181";
            //principalInfo.FirstName = "siva";
            //principalInfo.Surname = "kothamasu";
            //principalInfo.Designation = "123";

            //nonRegEntityRequest.PrincipalDetails = new PrincipalInfo[10];
            //nonRegEntityRequest.PrincipalDetails[0] = principalInfo;

            //principalInfo = new PrincipalInfo();
            //principalInfo.IDType = "1";
            //principalInfo.IdentityNumber = "6308305577083";
            //principalInfo.FirstName = "SAKHILE";
            //principalInfo.Surname = "KUNENE";
            //principalInfo.Designation = "123";

            //nonRegEntityRequest.PrincipalDetails[1] = principalInfo;
            //principalInfo = new PrincipalInfo();
            //principalInfo.IDType = "2";
            //principalInfo.IdentityNumber = "6009160136011";
            //principalInfo.FirstName = "Felicia";
            //principalInfo.Surname = "ADKINS";
            //principalInfo.Designation = "123";

            //nonRegEntityRequest.PrincipalDetails[2] = principalInfo;
            ////principalInfo = new PrincipalInfo();
            ////principalInfo.IDType = "1";
            ////principalInfo.IdentityNumber = "5203240071089";
            ////principalInfo.FirstName = "heila";
            ////principalInfo.Surname = "LOOTS";
            ////principalInfo.Designation = "123";

            ////nonRegEntityRequest.PrincipalDetails[3] = principalInfo;
            ////principalInfo = new PrincipalInfo();
            ////principalInfo.IDType = "1";
            ////principalInfo.IdentityNumber = "7806020158088";
            ////principalInfo.FirstName = "maria";
            ////principalInfo.Surname = "BEZUIDENHOUT";
            ////principalInfo.Designation = "123";

            ////nonRegEntityRequest.PrincipalDetails[4] = principalInfo;
            ////principalInfo = new PrincipalInfo();
            ////principalInfo.IDType = "1";
            ////principalInfo.IdentityNumber = "5911205865085";
            ////principalInfo.FirstName = "mandla";
            ////principalInfo.Surname = "MAVUSO";
            ////principalInfo.Designation = "123";

            ////nonRegEntityRequest.PrincipalDetails[5] = principalInfo;
            //nonRegEntityRequest.IsExistingClient = "2";

            //string toBeSend = JsonConvert.SerializeObject(nonRegEntityRequest);
            //using (WebClient client1 = new WebClient())
            //{
            //    client1.Headers[HttpRequestHeader.ContentType] = "application/json";
            //    var jsoncomObj = JsonConvert.SerializeObject(nonRegEntityRequest);
            //    //                var dataString = client1.UploadString("https://www.uat.xds.co.za/xdsconnectAPI/api/CustomVettingV", jsoncomObj);
            //    var dataString = client1.UploadString("http://localhost:7428/api/CustomVettingV", jsoncomObj);
            //}
            //var response = client.PostAsJsonAsync("api/ConnectCustomVettingSResult", toBeSend).Result;

            //Console.WriteLine(response.ToString());
            //if (response.IsSuccessStatusCode)
            //{
            //    // Parse the response body. Blocking!
            //    var dataObjects = response.Content.ReadAsAsync<IEnumerable<DataObject>>().Result;
            //    foreach (var d in dataObjects)
            //    {
            //        //Console.WriteLine("{0}", d.);
            //    }
            //}
            //else
            //{
            //    Console.WriteLine("{0} ({1})", (int)response.StatusCode, response.ReasonPhrase);
            //}  
        }
    }

    public class VettingVRequest
    {
        public VettingVRequest()
        {

        }
        string SearchTypeField;
        string SubjectNameField;
        string RegistrationNoField;
        string ITNumberField;
        string DunsNumberField;
        string VatNumberField;
        string BankAccountNumberField;
        string TradingNumberField;
        PrincipalInfo[] PrincipalInfoArrField = new PrincipalInfo[10];
        string UserField;
        string PasswordField;
        string ClientReferenceField;
        string isexistingclientFiled;
        public string SearchType
        {
            get { return SearchTypeField; }
            set { SearchTypeField = value; }
        }
        public string SubjectName
        {
            get { return SubjectNameField; }
            set { SubjectNameField = value; }
        }
        public string RegistrationNo
        {
            get { return RegistrationNoField; }
            set { RegistrationNoField = value; }
        }
        public string ITNumber
        {
            get { return ITNumberField; }
            set { ITNumberField = value; }
        }
        public string DunsNumber
        {
            get { return DunsNumberField; }
            set { DunsNumberField = value; }
        }
        public string VatNumber
        {
            get { return VatNumberField; }
            set { VatNumberField = value; }
        }
        public string BankAccountNumber
        {
            get { return BankAccountNumberField; }
            set { BankAccountNumberField = value; }
        }
        public string TradingNumber
        {
            get { return TradingNumberField; }
            set { TradingNumberField = value; }
        }
        public PrincipalInfo[] PrincipalDetails
        {
            get { return PrincipalInfoArrField; }
            set { PrincipalInfoArrField = value; }
        }
        public string User
        {
            get { return UserField; }
            set { UserField = value; }
        }
        public string Password
        {
            get { return PasswordField; }
            set { PasswordField = value; }
        }
        public string ClientReference
        {
            get { return ClientReferenceField; }
            set { ClientReferenceField = value; }
        }

        public string IsExistingClient
        {
            get { return this.isexistingclientFiled; }
            set { this.isexistingclientFiled = value; }
        }
    }

    public class PrincipalInfo
    {
        public PrincipalInfo()
        {

        }


        private string DesignationField;
        private string GrossMonthlyIncomeField;
        private string SurnameField;
        private string FirstNameField;
        private string IDTypeField;
        private string BirthDateField;
        private string IdentityNumberField;
        private string GenderField;
        private string MaritalStatusField;
        private string AddressLine1Field;
        private string AddressLine2Field;
        private string SuburbField;
        private string CityField;
        private string PostalCodeField;
        private string HomeTelephoneCodeField;
        private string HomeTelephoneNumberField;
        private string WorkTelephoneCodeField;
        private string WorkTelephoneNumberField;
        private string CellNumberField;
        private string OccupationField;
        private string EmployerField;
        private string NumberOfYearsAtEmployerField;
        private string BankNameField;
        private string BankAccountTypeField;
        private string BankBranchCodeField;
        private string BankAccountNumberField;
        private string HighestQualificationField;

        public string Designation
        {
            get { return DesignationField; }
            set { DesignationField = value; }
        }

        public string GrossMonthlyIncome
        {
            get { return GrossMonthlyIncomeField; }
            set { GrossMonthlyIncomeField = value; }
        }

        public string Surname
        {
            get { return SurnameField; }
            set { SurnameField = value; }
        }
        public string FirstName
        {
            get { return FirstNameField; }
            set { FirstNameField = value; }
        }
        public string IDType
        {
            get { return IDTypeField; }
            set { IDTypeField = value; }
        }
        public string BirthDate
        {
            get { return BirthDateField; }
            set { BirthDateField = value; }
        }
        public string IdentityNumber
        {
            get { return IdentityNumberField; }
            set { IdentityNumberField = value; }
        }
        public string Gender
        {
            get { return GenderField; }
            set { GenderField = value; }
        }
        public string MaritalStatus
        {
            get { return MaritalStatusField; }
            set { MaritalStatusField = value; }
        }
        public string AddressLine1
        {
            get { return AddressLine1Field; }
            set { AddressLine1Field = value; }
        }
        public string AddressLine2
        {
            get { return AddressLine2Field; }
            set { AddressLine2Field = value; }
        }
        public string Suburb
        {
            get { return SuburbField; }
            set { SuburbField = value; }
        }
        public string City
        {
            get { return CityField; }
            set { CityField = value; }
        }
        public string PostalCode
        {
            get { return PostalCodeField; }
            set { PostalCodeField = value; }
        }
        public string HomeTelephoneCode
        {
            get { return HomeTelephoneCodeField; }
            set { HomeTelephoneCodeField = value; }
        }
        public string HomeTelephoneNumber
        {
            get { return HomeTelephoneNumberField; }
            set { HomeTelephoneNumberField = value; }
        }
        public string WorkTelephoneCode
        {
            get { return WorkTelephoneCodeField; }
            set { WorkTelephoneCodeField = value; }
        }
        public string WorkTelephoneNumber
        {
            get { return WorkTelephoneNumberField; }
            set { WorkTelephoneNumberField = value; }
        }
        public string CellNumber
        {
            get { return CellNumberField; }
            set { CellNumberField = value; }
        }
        public string Occupation
        {
            get { return OccupationField; }
            set { OccupationField = value; }
        }
        public string Employer
        {
            get { return EmployerField; }
            set { EmployerField = value; }
        }
        public string NumberOfYearsAtEmployer
        {
            get { return NumberOfYearsAtEmployerField; }
            set { NumberOfYearsAtEmployerField = value; }
        }
        public string BankName
        {
            get { return BankNameField; }
            set { BankNameField = value; }
        }
        public string BankAccountType
        {
            get { return BankAccountTypeField; }
            set { BankAccountTypeField = value; }
        }
        public string BankBranchCode
        {
            get { return BankBranchCodeField; }
            set { BankBranchCodeField = value; }
        }
        public string BankAccountNumber
        {
            get { return BankAccountNumberField; }
            set { BankAccountNumberField = value; }
        }
        public string HighestQualification
        {
            get { return HighestQualificationField; }
            set { HighestQualificationField = value; }
        }
    }

    public class RegEntityResultInput
    {

        public int ProductCode { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
        public string TUNumber { get; set; }
        public string EnquiryReason { get; set; }
        public string EnquiryAmount { get; set; }
        public string Terms { get; set; }
        public string UserOnTicket { get; set; }
        public string ContactForename { get; set; }
        public string ContactSurname { get; set; }
        public string ContactPhoneCode { get; set; }
        public string ContactPhoneNo { get; set; }
        public string ContactFaxCode { get; set; }
        public string ContactFaxNo { get; set; }
        public string AdditionalClientReference { get; set; }
        public string SubjectPhoneCode { get; set; }
        public string SubjectPhoneNo { get; set; }
        public string SubjectNameOnTicket { get; set; }
        public string SubjectAddress { get; set; }
        public string SubjectSuburb { get; set; }
        public string SubjectCity { get; set; }
        public string SubjectPostCode { get; set; }
        public string InvestigateOption { get; set; }
        public string BankAccountNo { get; set; }
        public string BankAbbreviation { get; set; }
        public string BankBranch { get; set; }
        public string BankBranchCode { get; set; }
        public string SpecialInstructions { get; set; }
        public string BankCreditAmount { get; set; }
        public string BankTermsGiven { get; set; }
        public string BankAccountHolder { get; set; }
        public string IsExistingClient { get; set; }
    }


    public class RegEntityMatchInput
    {

        private string searchtypeField;

        private string subjectnameField;

        private string registrationnoField;

        private string itnumberField;

        private string dunsnumberField;

        private string vatnumberField;

        private string bankaccountnumberField;

        private string tradingnumberField;

        private string userField;

        private string passwordField;

        private string clientreferenceField;



        /// <remarks/>
        public string SearchType
        {
            get
            {
                return this.searchtypeField;
            }
            set
            {
                this.searchtypeField = value;
            }
        }

        /// <remarks/>
        public string SubjectName
        {
            get
            {
                return this.subjectnameField;
            }
            set
            {
                this.subjectnameField = value;
            }
        }

        /// <remarks/>
        public string RegistrationNo
        {
            get
            {
                return this.registrationnoField;
            }
            set
            {
                this.registrationnoField = value;
            }
        }

        /// <remarks/>
        public string ITNumber
        {
            get
            {
                return this.itnumberField;
            }
            set
            {
                this.itnumberField = value;
            }
        }

        /// <remarks/>
        public string DunsNumber
        {
            get
            {
                return this.dunsnumberField;
            }
            set
            {
                this.dunsnumberField = value;
            }
        }

        /// <remarks/>
        public string VatNumber
        {
            get
            {
                return this.vatnumberField;
            }
            set
            {
                this.vatnumberField = value;
            }
        }

        /// <remarks/>
        public string BankAccountNumber
        {
            get
            {
                return this.bankaccountnumberField;
            }
            set
            {
                this.bankaccountnumberField = value;
            }
        }

        /// <remarks/>
        public string TradingNumber
        {
            get
            {
                return this.tradingnumberField;
            }
            set
            {
                this.tradingnumberField = value;
            }
        }

        /// <remarks/>
        public string User
        {
            get
            {
                return this.userField;
            }
            set
            {
                this.userField = value;
            }
        }

        /// <remarks/>
        public string Password
        {
            get
            {
                return this.passwordField;
            }
            set
            {
                this.passwordField = value;
            }
        }

        /// <remarks/>
        public string ClientReference
        {
            get
            {
                return this.clientreferenceField;
            }
            set
            {
                this.clientreferenceField = value;
            }
        }

    }

    public class AMInput
    {
        public int ProductIndicator { get; set; }
        public string Company { get; set; }
        public string RuleSet { get; set; }
        public string Branch { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
        public string IsExistingClient { get; set; }
        public string GrossMonthlyIncome { get; set; }
        public string Surname { get; set; }
        public string FirstName { get; set; }
        public string IDType { get; set; }
        public string BirthDate { get; set; }
        public string IdentityNumber { get; set; }
        public string Gender { get; set; }
        public string MaritalStatus { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string Suburb { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        public string HomeTelephoneCode { get; set; }
        public string HomeTelephoneNumber { get; set; }
        public string WorkTelephoneCode { get; set; }
        public string WorkTelephoneNumber { get; set; }
        public string CellNumber { get; set; }
        public string Occupation { get; set; }
        public string Employer { get; set; }
        public string NumberOfYearsAtEmployer { get; set; }
        public string BankName { get; set; }
        public string BankAccountType { get; set; }
        public string BankBranchCode { get; set; }
        public string BankAccountNumber { get; set; }
        public string HighestQualification { get; set; }

    }

    public class MTNInput
    {

        private string companyField;

        private string ruleSetField;

        private string branchField;

        private string userField;

        private string passwordField;

        private string isExistingClientField;

        private string grossMonthlyIncomeField;

        private string surnameField;

        private string firstNameField;

        private string iDTypeField;

        private string birthDateField;

        private string identityNumberField;

        private string genderField;

        private string maritalStatusField;

        private string addressLine1Field;

        private string addressLine2Field;

        private string suburbField;

        private string cityField;

        private string postalCodeField;

        private string homeTelephoneCodeField;

        private string homeTelephoneNumberField;

        private string workTelephoneCodeField;

        private string workTelephoneNumberField;

        private string cellNumberField;

        private string occupationField;

        private string employerField;

        private string numberOfYearsAtEmployerField;

        private string bankNameField;

        private string bankAccountTypeField;

        private string bankBranchCodeField;

        private string bankAccountNumberField;

        private string highestQualificationField;

        private string dealerCodeField;

        private string customerNameField;

        private string channelField;

        private string alternatecontactnumberField;
        private string emailaddressField;

        /// <remarks/>
        public string Company
        {
            get
            {
                return this.companyField;
            }
            set
            {
                this.companyField = value;
            }
        }

        /// <remarks/>
        public string RuleSet
        {
            get
            {
                return this.ruleSetField;
            }
            set
            {
                this.ruleSetField = value;
            }
        }

        /// <remarks/>
        public string Branch
        {
            get
            {
                return this.branchField;
            }
            set
            {
                this.branchField = value;
            }
        }

        /// <remarks/>
        public string User
        {
            get
            {
                return this.userField;
            }
            set
            {
                this.userField = value;
            }
        }

        /// <remarks/>
        public string Password
        {
            get
            {
                return this.passwordField;
            }
            set
            {
                this.passwordField = value;
            }
        }

        /// <remarks/>
        public string IsExistingClient
        {
            get
            {
                return this.isExistingClientField;
            }
            set
            {
                this.isExistingClientField = value;
            }
        }

        /// <remarks/>
        public string GrossMonthlyIncome
        {
            get
            {
                return this.grossMonthlyIncomeField;
            }
            set
            {
                this.grossMonthlyIncomeField = value;
            }
        }

        /// <remarks/>
        public string Surname
        {
            get
            {
                return this.surnameField;
            }
            set
            {
                this.surnameField = value;
            }
        }

        /// <remarks/>
        public string FirstName
        {
            get
            {
                return this.firstNameField;
            }
            set
            {
                this.firstNameField = value;
            }
        }

        /// <remarks/>
        public string IDType
        {
            get
            {
                return this.iDTypeField;
            }
            set
            {
                this.iDTypeField = value;
            }
        }

        /// <remarks/>
        public string BirthDate
        {
            get
            {
                return this.birthDateField;
            }
            set
            {
                this.birthDateField = value;
            }
        }

        /// <remarks/>
        public string IdentityNumber
        {
            get
            {
                return this.identityNumberField;
            }
            set
            {
                this.identityNumberField = value;
            }
        }

        /// <remarks/>
        public string Gender
        {
            get
            {
                return this.genderField;
            }
            set
            {
                this.genderField = value;
            }
        }

        /// <remarks/>
        public string MaritalStatus
        {
            get
            {
                return this.maritalStatusField;
            }
            set
            {
                this.maritalStatusField = value;
            }
        }

        /// <remarks/>
        public string AddressLine1
        {
            get
            {
                return this.addressLine1Field;
            }
            set
            {
                this.addressLine1Field = value;
            }
        }

        /// <remarks/>
        public string AddressLine2
        {
            get
            {
                return this.addressLine2Field;
            }
            set
            {
                this.addressLine2Field = value;
            }
        }

        /// <remarks/>
        public string Suburb
        {
            get
            {
                return this.suburbField;
            }
            set
            {
                this.suburbField = value;
            }
        }

        /// <remarks/>
        public string City
        {
            get
            {
                return this.cityField;
            }
            set
            {
                this.cityField = value;
            }
        }

        /// <remarks/>
        public string PostalCode
        {
            get
            {
                return this.postalCodeField;
            }
            set
            {
                this.postalCodeField = value;
            }
        }

        /// <remarks/>
        public string HomeTelephoneCode
        {
            get
            {
                return this.homeTelephoneCodeField;
            }
            set
            {
                this.homeTelephoneCodeField = value;
            }
        }

        /// <remarks/>
        public string HomeTelephoneNumber
        {
            get
            {
                return this.homeTelephoneNumberField;
            }
            set
            {
                this.homeTelephoneNumberField = value;
            }
        }

        /// <remarks/>
        public string WorkTelephoneCode
        {
            get
            {
                return this.workTelephoneCodeField;
            }
            set
            {
                this.workTelephoneCodeField = value;
            }
        }

        /// <remarks/>
        public string WorkTelephoneNumber
        {
            get
            {
                return this.workTelephoneNumberField;
            }
            set
            {
                this.workTelephoneNumberField = value;
            }
        }

        /// <remarks/>
        public string CellNumber
        {
            get
            {
                return this.cellNumberField;
            }
            set
            {
                this.cellNumberField = value;
            }
        }

        /// <remarks/>
        public string Occupation
        {
            get
            {
                return this.occupationField;
            }
            set
            {
                this.occupationField = value;
            }
        }

        /// <remarks/>
        public string Employer
        {
            get
            {
                return this.employerField;
            }
            set
            {
                this.employerField = value;
            }
        }

        /// <remarks/>
        public string NumberOfYearsAtEmployer
        {
            get
            {
                return this.numberOfYearsAtEmployerField;
            }
            set
            {
                this.numberOfYearsAtEmployerField = value;
            }
        }

        /// <remarks/>
        public string BankName
        {
            get
            {
                return this.bankNameField;
            }
            set
            {
                this.bankNameField = value;
            }
        }

        /// <remarks/>
        public string BankAccountType
        {
            get
            {
                return this.bankAccountTypeField;
            }
            set
            {
                this.bankAccountTypeField = value;
            }
        }

        /// <remarks/>
        public string BankBranchCode
        {
            get
            {
                return this.bankBranchCodeField;
            }
            set
            {
                this.bankBranchCodeField = value;
            }
        }

        /// <remarks/>
        public string BankAccountNumber
        {
            get
            {
                return this.bankAccountNumberField;
            }
            set
            {
                this.bankAccountNumberField = value;
            }
        }

        /// <remarks/>
        public string HighestQualification
        {
            get
            {
                return this.highestQualificationField;
            }
            set
            {
                this.highestQualificationField = value;
            }
        }

        public string DealerCode
        {
            get
            {
                return this.dealerCodeField;
            }
            set
            {
                this.dealerCodeField = value;
            }
        }

        /// <remarks/>
        public string CustomerName
        {
            get
            {
                return this.customerNameField;
            }
            set
            {
                this.customerNameField = value;
            }
        }

        /// <remarks/>
        public string Channel
        {
            get
            {
                return this.channelField;
            }
            set
            {
                this.channelField = value;
            }
        }

        /// <remarks/>
        public string AlternateContactNumber
        {
            get
            {
                return this.alternatecontactnumberField;
            }
            set
            {
                this.alternatecontactnumberField = value;
            }
        }

        /// <remarks/>
        public string EmailAddress
        {
            get
            {
                return this.emailaddressField;
            }
            set
            {
                this.emailaddressField = value;
            }
        }
    }
}
